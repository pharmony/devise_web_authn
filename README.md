# devise_web_authn

A gem which allows Rails Devise users to authenticate against a second factor with WebAuthn API. We use [webauthn-ruby](https://github.com/cedarcode/webauthn-ruby) gem for WebAuthn relying party needs.

## Getting started
Devise Web Authn works with Rails 5.2 or newer and Devise 3.2 onwards. You need to add it to your application's Gemfile with:

```ruby
gem 'devise_web_authn'
```

Afterwards, run `bundle install` to install it.

Before being able to use it you need to set it up by running its installation generator:

```bash
$ rails generate devise_web-authn:install
```

During installation some instructions will be output to your console. Please follow these instructions carefully.
Specifically, you need to adapt your Devise models to include both the Web Authn registration and authentication modules. For example you need to add to `app/models/user.rb` the following lines:


```ruby
devise :web_authn_registerable, :web_authn_authenticatable', ...
```

Please ensure that the CSRF token check is always prepended on the action chain of your `ApplicationController`. Edit file `app/controllers/application_controller.rb` and change the `protect_from_forgery` line to include `prepend: true`:

```ruby
class ApplicationController < ActionController::Base
  # Prepend the verification of the CSRF token before the action chain.
  protect_from_forgery with: :exception, prepend: true
  ...
end
```

Now Devise with Web Authn is activated. Before using it, you need to migrate pending database changes by executing

```bash
$ rails db:migrate
```

This is not perfect but you need to add a field called `webauthn_id` on your devise models. Create a migration with this for your models and execute the migration (User, AdminUser, ...)

```ruby
class AddWebauthnIdToUsers < ActiveRecord::Migration[5.2]
  def change
    with_rsu do
      add_column :users, :webauthn_id, :string
      add_index :users, :webauthn_id, unique: true
    end
  end
end
```

Remember: To use it you always needs to run your development server with SSL. Otherwise, the Web Authn protocol will not allow registration or authentication!

## Multiple Relying Party

If you want to define multiple relying parties (for example you have different domains per users), you can use `WebAuthn::RelyingParty` by adding a method called `web_authn_relying_party` on your resource instance

```ruby
  def web_authn_relying_party
    WebAuthn::RelyingParty.new(
      origin: "https://subdomain.pharmony.eu",
      name: 'Pharmony ONE Subdomain'
    )
  end
```

If this method is not define, this gem will use the default.

## Web Authn Views

To enable the user to register a FIDO U2F device and to change the appeareance of the authentication screens you need to customize its views.
You can install the `devise_web_authn` views by running

```bash
rails generate devise_web_authn:views
```

## License
The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

