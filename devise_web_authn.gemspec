$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "devise_web_authn/version"

Gem::Specification.new do |s|
  s.name        = 'devise_web_authn'
  s.version     = DeviseWebAuthn::VERSION
  s.summary     = "A Devise module to allow Web Authn authentication."
  s.description = "Enables a Rails Devise app to authenticate users with a Web Authn second factor."
  s.authors     = ["Pharmony developers"]
  s.email       = 'dev@pharmony.lu'
  s.files       = ["lib/hola.rb"]
  s.homepage    = 'https://gitlab.com/pharmony/devise_web_authn'
  s.license     = 'MIT'

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "README.md"]

  s.add_dependency 'devise', '>= 3.2'
  s.add_dependency 'rails', '>= 5.2'
  s.add_dependency 'thor', '>= 0.20.0'
  s.add_dependency 'webauthn', '>= 3.0.0.alpha1'
end
