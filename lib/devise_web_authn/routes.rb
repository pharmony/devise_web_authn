module ActionDispatch
  module Routing
    #
    # Routes to register WebAuthn devices and to authenticate with a WebAuthn device
    #
    class Mapper
      def devise_web_authn_registration(mapping, controllers)
        resource :web_authn_registration,
                 only: %i[new create],
                 path: mapping.path_names[:web_authn_registration],
                 controller: controllers[:web_authn_registrations]
      end

      def devise_web_authn_authentication(mapping, controllers)
        resource :web_authn_authentication,
                 only: %i[new create],
                 path: mapping.path_names[:web_authn_authentication],
                 controller: controllers[:web_authn_authentications]
      end
    end
  end
end
