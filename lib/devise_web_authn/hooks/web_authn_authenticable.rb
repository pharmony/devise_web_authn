Warden::Manager.after_authentication do |user, auth, options|
  web_authn_enabled = true
  web_authn_enabled = user.web_authn_enabled? if user.respond_to?(:web_authn_enabled?)

  if web_authn_enabled
    if user.respond_to?(:with_web_authn_authentication?)
      with_web_authn_authentication = user.with_web_authn_authentication?
      scope = auth.session(options[:scope])
      scope[:with_web_authn_authentication] = with_web_authn_authentication

      scope[:id] = user.id if with_web_authn_authentication
    end
  end
end
