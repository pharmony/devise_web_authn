module DeviseWebAuthn
  class Engine < ::Rails::Engine
    ActiveSupport.on_load(:action_controller) do
      include DeviseWebAuthn::Controllers::Helpers
    end
  end
end
