module Devise
  module Models
    module WebAuthnRegisterable
      extend ActiveSupport::Concern

      included do
        has_many :webauthn_credentials,
                 as: :user,
                 class_name: 'WebauthnCredential',
                 dependent: :destroy
      end
    end
  end
end
