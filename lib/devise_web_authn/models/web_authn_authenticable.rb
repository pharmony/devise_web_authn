require 'devise_web_authn/hooks/web_authn_authenticable'

module Devise
  module Models
    module WebAuthnAuthenticable
      extend ActiveSupport::Concern

      # Does the user has a registered Web Authn device?
      def with_web_authn_authentication?
        WebauthnCredential.where(user: self).count > 0
      end
    end
  end
end
