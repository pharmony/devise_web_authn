class Create<%= table_name.camelize %> < ActiveRecord::Migration<%= migration_version %>
  def change
    create_table :<%= table_name %> do |t|
      t.references  :user,    null: false, polymorphic: true, index: true
      t.string :external_id,  null: false
      t.string :public_key,   null: false, limit: 2048
      t.string :nickname,     null: false
      t.integer :sign_count,  null: false, default: 0

<% attributes.each do |attribute| -%>
      t.<%= attribute.type %> :<%= attribute.name %>
<% end -%>
      t.timestamps
    end
    add_index :<%= table_name %>, :external_id, unique: true
    add_index :<%= table_name %>, %i[nickname user_id], unique: true
  end
end
