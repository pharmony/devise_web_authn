module DeviseWebAuthn
  module Controllers
    autoload :Helpers, 'devise_web_authn/controllers/helpers'
  end
end

require 'devise'
require 'webauthn'
require 'devise_web_authn/routes'
require 'devise_web_authn/rails'

Devise.add_module :web_authn_registerable, :model => 'devise_web_authn/models/web_authn_registerable', :controller => :web_authn_registrations, :route => :web_authn_registration
Devise.add_module :web_authn_authenticable, :model => 'devise_web_authn/models/web_authn_authenticable', :controller => :web_authn_authentications, :route => :web_authn_authentication
