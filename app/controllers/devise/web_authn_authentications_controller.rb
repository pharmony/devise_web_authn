class Devise::WebAuthnAuthenticationsController < DeviseController
  before_action :find_resource_and_verify_password, only: %i[new create]

  def new
    hash = {
      allow: @resource.webauthn_credentials.map { |c| c.external_id },
      user_verification: user_verification
    }
    if relying_party.present?
      hash[:relying_party] = relying_party
    end
    options = WebAuthn::Credential.options_for_get(hash)
    session[:web_authn_authentication_challenge] = options.challenge
    @options = options
    render :new
  end

  def create
    credential = JSON.parse(params[:credential])
    webauthn_credential = nil
    if relying_party.present?
      webauthn_credential = WebAuthn::Credential.from_get(
        credential,
        relying_party: relying_party
      )
    else
      webauthn_credential = WebAuthn::Credential.from_get(
        credential
      )
    end

    stored_credential = @resource.webauthn_credentials.find_by(external_id: webauthn_credential.id)

    begin
      webauthn_credential.verify(
        session[:web_authn_authentication_challenge],
        public_key: stored_credential.public_key,
        sign_count: stored_credential.sign_count,
        user_verification: false
#        user_verification: (user_verification.to_s == 'discouraged' ? false : true)
      )

      stored_credential.update!(sign_count: webauthn_credential.sign_count)
      # Remember the user (if applicable)
      @resource.remember_me = Devise::TRUE_VALUES.include?(session[:"#{resource_name}_remember_me"]) if @resource.respond_to?(:remember_me=)
      sign_in(resource_name, @resource)

    rescue WebAuthn::SignCountVerificationError => e
      @error_message = "Unable to authenticate: #{e.message}"
      flash[:error] = @error_message
      Rails.logger.error e
      Rails.logger.debug e.backtrace
    rescue WebAuthn::Error => e
      @error_message = "Unable to authenticate: #{e.message}"
      flash[:error] = @error_message
      Rails.logger.error e
      Rails.logger.debug e.backtrace
    ensure
      session.delete(:web_authn_authentication_challenge)
    end

    respond_with resource, location: after_sign_in_path_for(@resource)
  end

  private

  def user_verification
    resource.respond_to?(:web_authn_user_verification) ? resource.web_authn_user_verification : 'discouraged'
  end

  def find_resource_and_verify_password
    @resource = send("current_#{resource_name}")
    if @resource.nil?
      @resource = resource_class.find_by_id(session["#{resource_name}_id"])
    end
    if @resource.nil? || !Devise::TRUE_VALUES.include?(session[:"#{resource_name}_password_checked"])
      redirect_to root_path
    end
  end

  def relying_party
    @resource.respond_to?(:web_authn_relying_party) ? @resource.web_authn_relying_party : nil
  end

end
