class Devise::WebAuthnRegistrationsController < ApplicationController
  before_action :authenticate_resource!

  def new
    # Generate and store the WebAuthn User ID the first time the user registers a credential
    if !resource.webauthn_id
      resource.update!(webauthn_id: WebAuthn.generate_user_id)
    end

    hash = {
      user: {
        id: resource.webauthn_id,
        display_name: resource.email, # we have only the email
        name: resource.email # we have only the email
      },
      exclude: resource.webauthn_credentials.pluck(:external_id),
      authenticator_selection: {
        user_verification: user_verification
      }
    }

    if relying_party.present?
      hash[:relying_party] = relying_party
    end
    options = WebAuthn::Credential.options_for_create(hash)
    session[:web_authn_credential_register_challenge] = options.challenge

    @options = options

    render :new
  end

  def create
    credential = JSON.parse(params[:credential])

    # Create WebAuthn Credentials from the request params
    webauthn_credential = nil
    if relying_party.present?
      webauthn_credential = WebAuthn::Credential.from_create(
        credential,
        relying_party: relying_party
      )
    else
      webauthn_credential = WebAuthn::Credential.from_create(
        credential
      )
    end

    begin
      # Validate the challenge
      webauthn_credential.verify(
        session[:web_authn_credential_register_challenge],
        user_verification: false
#        user_verification: (user_verification.to_s == 'discouraged' ? false : true)
      )

      # The validation would raise WebAuthn::Error so if we are here, the credentials are valid, and we can save it
      credential = resource.webauthn_credentials.new(
        external_id: webauthn_credential.id,
        public_key: webauthn_credential.public_key,
        nickname: params[:nickname] || "credential_#{SecureRandom.uuid}",
        sign_count: webauthn_credential.sign_count
      )

      unless credential.save
        raise Exception, credential.errors.inspect
      end
    rescue WebAuthn::Error => e
      Rails.logger.error e
      Rails.logger.debug e.backtrace
      @error_message = "Unable to register: #{e.message}"
      flash[:error] = @error_message
    ensure
      session.delete(:web_authn_credential_register_challenge)
    end

    respond_to do |format|
      format.js
      format.html { redirect_to root_url }
    end
  end

  if respond_to?(:helper_method)
    helpers = %w[resource_name]
    helper_method(*helpers)
  end

  protected

  def resource_name
    devise_mapping.name
  end

  def devise_mapping
    @devise_mapping ||= request.env['devise.mapping']
  end

  private

  def relying_party
    resource.respond_to?(:web_authn_relying_party) ? resource.web_authn_relying_party : nil
  end

  def user_verification
    resource.respond_to?(:web_authn_user_verification) ? resource.web_authn_user_verification : 'discouraged'
  end

  def resource
    send("current_#{resource_name}")
  end

  def authenticate_resource!
    send("authenticate_#{resource_name}!")
  end

  def fido_usf_registration_url
    params[:on_success_redirect_to].presence ||
      send("#{resource_name}_web_authn_registration_url")
  end
end
