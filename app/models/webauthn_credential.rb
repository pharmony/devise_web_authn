class WebauthnCredential < ActiveRecord::Base
  belongs_to :user, polymorphic: true

  validates :user, presence: true
  validates :external_id, presence: true, uniqueness: true
  validates :public_key, presence: true
  validates :nickname, presence: true, uniqueness: { scope: :user_id }
  validates :sign_count, presence: true
end
